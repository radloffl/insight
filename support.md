---
layout: page
title: Need Help?
permalink: /support/
weight: 3
screenshot_folder: /assets/support-screenshots
---

## Settings Screen

{::nomarkdown}
<button class="accordion">How do I get to the settings screen?</button>
<div class="panel">
<img alt="Screenshot demonstrating how to get to the settings screen" src="{{ page.screenshot_folder }}/go_to_settings.png" title="Go to settings screenshot">
<blockquote> 
<ul>
<li>Tap Settings (upper left)</li>
<li>Or tap the blue label with the current time settings</li>
</ul>
</blockquote>
</div>
{:/}

---

{::nomarkdown}
<button class="accordion">How I do change the timer?</button>
<div class="panel">
<img alt="Screenshot demonstrating how to change the timer" src="{{ page.screenshot_folder }}/change_timer.png" title="Change timer screenshot">
<blockquote> 
<ul>
<li>Tap the first row with the time labels</li>
<li>Use the spinners to select your desired time</li>
<li>Tap Done (upper right)</li>
</ul>
</blockquote>
</div>
{:/}

---

{::nomarkdown}
<button class="accordion">How do I change labels?</button>
<div class="panel">
<img alt="Screenshot demonstrating how to change labels" src="{{ page.screenshot_folder }}/change_labels.png" title="Change labels screenshot">
<blockquote> 
<ul>
<li>Tap in a row to edit a label (brings up keyboard)</li>
<li>Tap red remove button then tap delete to remove a label</li>
<li><ul><li>Minimum of 2</li></ul></li>
<li>Tap the + button to add a new label</li>
<li><ul><li>Maximum of 6</li></ul></li>
<li>Tap Done (upper right)</li>
</ul>
</blockquote>
</div>
{:/}

---

{::nomarkdown}
<button class="accordion">How do I change how I am notified about interval changes?</button>
<div class="panel">
<img alt="Screenshot demonstrating how to change switches" src="{{ page.screenshot_folder }}/change_audio_and_haptic.png" title="Change default screenshot">
<blockquote> 
<ul>
<li>Tap switch on row to change</li>
<li>
<ul>
    <li>Green and slid right = On</li>
    <li>White and slid left = Off</li>
</ul>
</li>
</ul>
</blockquote>
</div>
{:/}

---

{::nomarkdown}
<button class="accordion">How do I change the settings when the app is next opened?</button>
<div class="panel">
<img alt="Screenshot demonstrating how to set default settings" src="{{ page.screenshot_folder }}/change_default.png" title="Change default screenshot">
<blockquote> 
<ul>
<li>Change the settings as desired</li>
<li>Tap the row with Make default</li>
<li>Tap Yes to confirm your changes</li>
<li>Tap OK</li>
</ul>
</blockquote>
</div>
{:/}

---
## Timer Screen
---

{::nomarkdown}
<button class="accordion">How do I start the timer?</button>
<div class="panel">
<img alt="Screenshot demonstrating how to start the timer" src="{{ page.screenshot_folder }}/start_timer.png" title="Start timer screenshot">
<blockquote> 
<ul>
<li>Tap Start (lower left)</li>
</ul>
</blockquote>
</div>
{:/}

---

{::nomarkdown}
<button class="accordion">How do I reset (or pause) the timer screen?</button>
<div class="panel">
<img alt="Screenshot demonstrating how to reset or pause the timer screen" src="{{ page.screenshot_folder }}/pause_and_reset.png" title="Pause or reset timer screen screenshot">
<blockquote> 
<ul>
<li>Tap Reset (lower right)</li>
<li><ul><li>Tap Time and observations</li><li>Tap Time only</li></ul></li>
<li>Tap Pause</li>
</ul>
</blockquote>
</div>
{:/}

---

{::nomarkdown}
<button class="accordion">How do I mark or unmark an observation?</button>
<div class="panel">
<img alt="Screenshot demonstrating how to mark and unmark observation" src="{{ page.screenshot_folder }}/mark_and_unmark.png" title="Mark and unmark screenshot">
<blockquote> 
<ul>
<li>Tap the label</li>
</ul>
</blockquote>
</div>
{:/}

---

{::nomarkdown}
<head>
<script type="text/javascript">
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].onclick = function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  }
}
</script>
</head>
{:/}  