---
layout: page
title: About
permalink: /about/
weight: 2
---

### The App
{{ site.app_name }} started when my girlfriend explained how tedious it was to record interval observations. She is a student in a school psychology program and had to complete the task often. I began making the app as a tool for her to replace pen and paper. After a few iterations of the prototype and her showing the app to a few classmates it became obvious {{ site.app_name }} could be a useful tool for anyone doing interval observations and shouldn't be kept a secret! I have since continued devloping {{ site.app_name }} in my free time.

### The Developer
#### Logan Radloff
Hey! I'm Logan, I am a Senior at the University of Northern Iowa majoring in Computer Science. I am a full time student and hold multiple part time jobs. I build {{ site.app_name }} in my free time! 

![Olivia and I (Logan)]({{ site.baseurl }}/assets/olivia_and_i.jpg)